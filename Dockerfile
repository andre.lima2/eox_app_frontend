# IMPORTANT: before executing "docker build" command it is necessary to
# 1 - edit axios.defaults.baseURL in src/boot/axios.js with hostname or IP address where the backend REST API is hosted in
# 2 - execute "quasar build --modern"

FROM alpine:3.12.3

EXPOSE 4000

RUN apk add --update nodejs npm &&  \
    npm install -g @quasar/cli

COPY /dist /dist
WORKDIR /

CMD quasar serve /dist/spa


const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/eox_api/ByDates',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/eox_api/ByDates.vue') }
    ]
  },
  {
    path: '/eox_api/ByPID',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/eox_api/ByProductID.vue') }
    ]
  },
  {
    path: '/eox_api/BySerialNumber',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/eox_api/BySerialNumber') }
    ]
  },
  {
    path: '/eox_api/BySWRelease',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/eox_api/BySWRelease') }
    ]
  },
  {
    path: '/management/DomainsManagement',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/management/DomainsManagement.vue') }
    ]
  },
  {
    path: '/management/AssetsManagement',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/management/AssetsManagement.vue') }
    ]
  },
  {
    path: '/management/Reports',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/management/Reports.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes

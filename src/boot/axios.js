// import axios from 'axios'
// const axiosInstance = axios.create({
//   baseURL: 'http://127.0.0.1:5000'
// })
// export default ({ Vue }) => {
//   Vue.prototype.$axios = axios
// }
// export { axiosInstance }


import axios from 'axios'

export default async ({ Vue }) => {
  axios.defaults.baseURL = "http://localhost:5000";


  Vue.prototype.$axios = axios
}

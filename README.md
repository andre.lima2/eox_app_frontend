# EoX Portal Frontend

Provides EoX lookup functionality and reports. To further information, refer to the [EoX Portal REST API](https://gitlab.com/andre.lima2/eoxv5-client/-/blob/master/README.md)

### Required packages
In theory, once 'quasar build --modern' command is issued, and the production ready bundle is available, no specific software is needed (other than the webserver itself, of course). However, assuming npm and quasar commands will be executed in the same host from which the files will be served, the following packages are necessary:

- nodejs >= 10 & npm
- Quasar (requires npm):
```bash
npm install -g @quasar/cli
```

### Install the dependencies
cd into the eox_app_frontend directory
```bash
npm install
```

### Edit default URL for back end REST API
The means by which the front end app knows how to reach the back end REST API is the value assigned to *axios.defaults.baseURL* inside 'src/boot/axios.js' file.
That URL is currently configured as "http://localhost:5000", and you will have to change it to the hostname/IP:port according to your scenario.

### (Optional) Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Production deployment
Details of production deployment of a quasar project can be found in [Official Documentation](https://quasar.dev/quasar-cli/developing-spa/deploying)

By far, the easiest way is to deploy to production is with quasar itself.
First:
```bash
quasar build --modern
```
This will create a /dist/spa structure, in which all production files will be available. Once it's done, you can serve from that directory:
```bash
quasar serve /path_to_dist/dist/spa
```

### Containerized Deployment
- If you wish to deploy this frontend app as a standalone Docker container, check the Dockerfile available in the root directory.
- If you wish to have both front and backend applications deployed as containers, check the docker-compose.yml files [available here](https://still.pending.com).


### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
